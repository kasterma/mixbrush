/*
 * SyntaxHighlighter is at http://alexgorbatchev.com/SyntaxHighlighter
 * by Alex Gorbatchev
 *
 * This Brush for MIX is by Bart Kastermans, www.bartk.nl
 *
 * Instructions for writing a Brush are at 
 * http://alexgorbatchev.com/SyntaxHighlighter/manual/brushes/custom.html
 * unfortunately these turned out not enough to write this for MIX.  By
 * looking through the code of shCore.js we found out how to deal with the
 * end of line comments.
 */

SyntaxHighlighter.brushes.MIX = function()
{
  var keywords  = 'equ alf sta stx st1 st2 st3 st4 st5 st6 lda ldx ld1 ld2 ld3 ld4 ' +
                  'ld5 ld6 move con stj jmp je jl out enta ent1 ent2 ent3 ent4 ent5 ' +
                  'ent6 deca decx dec1 dec2 dec3 dec4 dec5 dec6 inca incx inc1 inc2 ' +
                  'inc3 inc4 inc5 inc6 cmpa cmpx cmp1 cmp2 cmp3 cmp4 cmp5 cmp6 hlt ' +
                  'char num orig add mul jge jne sra sub src slc entx end';

  function reworkmatch (match, regexInfo)
  {
      // for end of line comments on lines that start with whitespace
      var re_sws = new RegExp ('^\\s+\\S+\\s+\\S+');
      // for end of lien comments on lines that start with a label
      var re_snws = new RegExp ('^\\S+\\s+\\S+\\s+\\S+');

      // try to match both
      var rem = re_sws.exec (match [0]);
      var rem2 = re_snws.exec (match [0]);

      // if neither matches, then this line is of special form
      // (in my files this was the case for a HLT instruction on a line by itself)
      var comment_start = 1000;   // longer than any line that would appear in the file

      if (rem)
          {
              comment_start = rem[0].length;
          }
      else if (rem2)
          {
              comment_start = rem2[0].length;
          };

      // update the match object to only match the end of line comment
      match.index = match.index + comment_start;
      return match[0].slice (comment_start);
  };

  this.regexList = [
       // comments starting at the begining of the line
       { regex: new RegExp('^[*](.*)$', 'gm'), css: 'comments' },
       // quoted strings
       { regex: SyntaxHighlighter.regexLib.doubleQuotedString, css: 'string' },
       // labels
       { regex: new RegExp('^[A-Z0-9][^ ]*', 'gm'), css: 'color2' },
       // keywords
       { regex: new RegExp(this.getKeywords(keywords), 'gmi'), css: 'keyword' },
       // comments at the end of the line
       { regex: new RegExp('^[a-zA-Z0-9* ](.*)$', 'gm'), css: 'comments', func: reworkmatch }
                    ];
};
SyntaxHighlighter.brushes.MIX.prototype = new SyntaxHighlighter.Highlighter();

SyntaxHighlighter.brushes.MIX.aliases  = ['mix', 'MIX'];
